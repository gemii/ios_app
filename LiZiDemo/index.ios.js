/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
 import React, { Component } from 'react';

 import {
   AppRegistry,
   StyleSheet,
   Text,
   View,
   TouchableHighlight,
   ScrollView,
   ListView,
 } from 'react-native';

 import { NativeModules } from 'react-native';
 import { NativeAppEventEmitter } from 'react-native';

 // var subscription;
 var RootViewController = NativeModules.RootViewController;
 var RNBridgeModule = NativeModules.RNBridgeModule;

 class CustomButton extends React.Component {
   render() {
     return (
       <TouchableHighlight
         style={styles.button}
         underlayColor="#a5a5a5"
         onPress={this.props.onPress}>
         <Text style={styles.buttonText}>{this.props.text}</Text>
       </TouchableHighlight>
     );
   }
 }

var i = 10;
 class HelloWorld extends Component {
   // 初始化模拟数据
   // 初始化模拟数据
    //
    // console.log(i);
    //
    //  constructor(props) {
    //    super(props);
    //    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    //    this.state = {
    //      dataSource: ds.cloneWithRows([
    //        'John', 'Joel', 'James', 'Jimmy', 'Jackson', 'Jillian', 'Julie', 'Devin'
    //      ])
    //    };
    //  }
    //  render() {
    //    return (
    //      <View style={{paddingTop: 22}}>
    //        <ListView
    //          dataSource={this.state.dataSource}
    //          renderRow={(rowData) => <Text>{rowData}</Text>}
    //        />
    //      </View>
    //    );
    //  }
   render() {
     return (
       <ScrollView alwaysBounceVertical={false} style={{flex:1}}>
         <CustomButton text="返回"
             onPress={()=>RootViewController.addEventDismiss()}
         />
         <CustomButton text="一周"
             onPress={()=>RootViewController.addEvent('1')}
         />
         <CustomButton text="二周"
             onPress={()=>RootViewController.addEvent('2')}
         />
         <CustomButton text="三周"
             onPress={()=>RootViewController.addEvent('3')}
         />
         <CustomButton text="四周"
             onPress={()=>RootViewController.addEvent('4')}
         />
         <CustomButton text="五周"
             onPress={()=>RootViewController.addEvent('5')}
         />
         <CustomButton text="六周"
             onPress={()=>RootViewController.addEvent('6')}
         />
         <CustomButton text="七周"
             onPress={()=>RootViewController.addEvent('7')}
         />
         <CustomButton text="八周"
             onPress={()=>RootViewController.addEvent('8')}
         />
         <CustomButton text="九周"
             onPress={()=>RootViewController.addEvent('9')}
         />
         <CustomButton text="十周"
             onPress={()=>RootViewController.addEvent('10')}
         />
         <CustomButton text="十一周"
             onPress={()=>RootViewController.addEvent('11')}
         />
         <CustomButton text="十二周"
             onPress={()=>RootViewController.addEvent('12')}
         />
         <CustomButton text="十三周"
             onPress={()=>RootViewController.addEvent('13')}
         />
         <CustomButton text="十四周"
             onPress={()=>RootViewController.addEvent('14')}
         />
         <CustomButton text="十五周"
             onPress={()=>RootViewController.addEvent('15')}
         />
         <CustomButton text="十六周"
             onPress={()=>RootViewController.addEvent('16')}
         />
         <CustomButton text="十七周"
             onPress={()=>RootViewController.addEvent('17')}
         />
         <CustomButton text="十八周"
             onPress={()=>RootViewController.addEvent('18')}
         />
         <CustomButton text="十九周"
             onPress={()=>RootViewController.addEvent('19')}
         />
         <CustomButton text="二十周"
             onPress={()=>RootViewController.addEvent('20')}
         />
       </ScrollView>
     );
   }
}

//样式
 const styles = StyleSheet.create({
   dismissBtn :{
     backgroundColor: 'blue',
     height: 20,
     top: 10,
     width: 20,
     left: 200,
   },
   welcome: {
     fontSize: 20,
     textAlign: 'center',
     margin: 10,
   },
   button: {
     top: 50,
     width: 70,
     height: 70,
     left: 170,
     padding: 20,
     borderWidth:1,
     borderColor: '#cdcdcd',
     borderRadius: 35,
     backgroundColor: '#ffffff',
     marginTop:20,
   },
   firstButton: {
     top:20,
     margin:5,
     backgroundColor: 'white',
     padding: 10,
     borderWidth:1,
     borderColor: '#cdcdcd',
   },
 });


AppRegistry.registerComponent('HelloWorld', () => HelloWorld);
