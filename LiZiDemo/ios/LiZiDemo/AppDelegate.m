/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"


#import <AFNetworking.h>
#import "GameViewController.h"
#import "UMComSession.h"
#import <SMS_SDK/SMSSDK.h>
#import <UMSocial.h>
#import <UMSocialWechatHandler.h>
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
  NSString *docPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
  NSLog(@"%@", docPath);

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  
  GameViewController *rootVC = [GameViewController new];
 
  self.window.rootViewController = rootVC;
  [self.window makeKeyAndVisible];
  
  
  [UMSocialData setAppKey:@"57b17f5a67e58ef25f00268c"];
  
  [[UMComSession sharedInstance] setAppkey:@"57b17f5a67e58ef25f00268c" withAppSecret:@"5a97893c9b9a527e61591a687f03e8e7"];
  
  //初始化应用，appKey和appSecret从后台申请得
  [SMSSDK registerApp:@"164624275889c"
           withSecret:@"1d9ae7e58276db4e79542b2a02bed07a"];
  
  //隐藏状态栏
//  [[UIApplication sharedApplication] setStatusBarHidden:YES];
  
  return YES;
}



@end
