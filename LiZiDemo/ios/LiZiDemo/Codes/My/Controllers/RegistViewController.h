//
//  RegistViewController.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/19.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *countTF;

@property (weak, nonatomic) IBOutlet UITextField *smsTF;

@property (weak, nonatomic) IBOutlet UITextField *nameTF;

@property (weak, nonatomic) IBOutlet UITextField *passwordTF;

@property (weak, nonatomic) IBOutlet UITextField *confirmTF;

@property (weak, nonatomic) IBOutlet UITextField *phoneTF;



@end
