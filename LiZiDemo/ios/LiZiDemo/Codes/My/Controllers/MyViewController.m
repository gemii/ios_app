//
//  MyViewController.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/17.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "MyViewController.h"
#import "NiceAlertSheet.h"
@interface MyViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) UILabel *nameLabel;

@property (nonatomic, weak) UIView *topContainer;

@property (nonatomic, weak) UIImageView *headView;

@property (nonatomic, weak) UIView *addTagView;

@property (nonatomic, weak) UIView *selectView;

@end

@implementation MyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.view.backgroundColor = [UIColor whiteColor];
  
  [self createSubViews];
  
}

- (void)createSubViews {
  
  UIImageView *headView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"头像"]];
  headView.layer.cornerRadius = kScreenWidth * 0.15;
  headView.clipsToBounds = YES;
  headView.userInteractionEnabled = YES;
  [headView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap)]];
  
  
  UILabel *nameLabel = [[UILabel alloc] init];
  nameLabel.text = @"Sking";
  nameLabel.userInteractionEnabled = YES;
  nameLabel.textAlignment = NSTextAlignmentCenter;
  nameLabel.font = [UIFont boldSystemFontOfSize:19];
  self.nameLabel = nameLabel;
//  UIView *topContainView = [[UIView alloc] init];
  
  [self.view sd_addSubviews:@[headView, nameLabel]];
  
  self.headView = headView;
  headView.sd_layout
  .centerXEqualToView(self.view)
  .topSpaceToView(self.view, 20)
  .widthIs(kScreenWidth * 0.3)
  .heightIs(kScreenWidth * 0.3);
  
  nameLabel.sd_layout
  .topSpaceToView(headView, 10)
  .centerXEqualToView(self.view)
  .widthIs(kScreenWidth * 0.5)
  .heightIs(21);
  
  NSArray *tagArr = @[@"标签一", @"标签二", @"标签三", @"标签四", @"标签五", @"标签六"];
  
  [self setupAutoWidthViewsWithCount:tagArr.count margin:10 tagArr:tagArr];
  
  //底部容器
  UIView *bottomContainer = [[UIView alloc] init];
//  bottomContainer.backgroundColor = [UIColor lightGrayColor];
  [self.view addSubview:bottomContainer];
  
  //修改个人信息按钮
  UILabel *editLabel = [[UILabel alloc] init];
  editLabel.text = @"修改个人信息";
  editLabel.userInteractionEnabled = YES;
  [editLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editClick)]];
  editLabel.textColor = [UIColor darkGrayColor];
  editLabel.textAlignment = NSTextAlignmentRight;
  [self.view addSubview:editLabel];
  
  bottomContainer.sd_layout
  .leftSpaceToView(self.view, 35)
  .rightSpaceToView(self.view, 35)
  .bottomSpaceToView(self.view, 20)
  .topSpaceToView(self.topContainer, kScreenHeight *0.12);
  
  editLabel.sd_layout
  .bottomSpaceToView(bottomContainer, 2)
  .rightEqualToView(bottomContainer)
  .heightIs(21)
  .widthIs(150);
  
  //左边label
  UILabel *momAgeLabel = [[UILabel alloc] init];
  UILabel *babyNameLabel = [[UILabel alloc] init];
  UILabel *babyGenderLabel = [[UILabel alloc] init];
  UILabel *babyBirthLabel = [[UILabel alloc] init];
  UILabel *wechatLabel = [[UILabel alloc] init];
  
  //右边label
  UILabel *momAgeLabelR = [[UILabel alloc] init];
  UILabel *babyNameLabelR = [[UILabel alloc] init];
  UILabel *babyGenderLabelR = [[UILabel alloc] init];
  UILabel *babyBirthLabelR = [[UILabel alloc] init];
  UILabel *wechatLabelR = [[UILabel alloc] init];
  
  
  
  momAgeLabel.text = @"妈妈年龄段";
  babyNameLabel.text = @"宝宝昵称";
  babyGenderLabel.text = @"宝宝性别";
  babyBirthLabel.text = @"宝宝生日";
  wechatLabel.text = @"微信号";
  
  momAgeLabelR.text = @"20~25";
  babyNameLabelR.text = @"李狗蛋";
  babyGenderLabelR.text = @"男宝宝";
  babyBirthLabelR.text = @"16/8/30";
  wechatLabelR.text = @"Sking27149";
  
  //退出登录按钮
  UIButton *logoutBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  [logoutBtn setTitle:@"退出登录" forState:(UIControlStateNormal)];
  [logoutBtn addTarget:self action:@selector(logoutClick) forControlEvents:(UIControlEventTouchUpInside)];
  logoutBtn.backgroundColor = [UIColor redColor];
  logoutBtn.layer.cornerRadius = 15;
  
  [bottomContainer sd_addSubviews:@[momAgeLabel, babyNameLabel, babyGenderLabel, babyBirthLabel, wechatLabel, momAgeLabelR, babyNameLabelR, babyGenderLabelR, babyBirthLabelR, wechatLabelR, logoutBtn]];
  
  
  
  momAgeLabel.sd_layout
  .leftSpaceToView(bottomContainer, 5)
  .topSpaceToView(bottomContainer, 5)
  .heightIs(25)
  .widthIs(150);
  
  babyNameLabel.sd_layout
  .leftSpaceToView(bottomContainer, 5)
  .topSpaceToView(momAgeLabel, 15)
  .heightIs(25)
  .widthIs(150);
  
  babyGenderLabel.sd_layout
  .leftSpaceToView(bottomContainer, 5)
  .topSpaceToView(babyNameLabel, 15)
  .heightIs(25)
  .widthIs(150);
  
  babyBirthLabel.sd_layout
  .leftSpaceToView(bottomContainer, 5)
  .topSpaceToView(babyGenderLabel, 15)
  .heightIs(25)
  .widthIs(150);
  
  wechatLabel.sd_layout
  .leftSpaceToView(bottomContainer, 5)
  .topSpaceToView(babyBirthLabel, 15)
  .heightIs(25)
  .widthIs(150);
  
  momAgeLabelR.sd_layout
  .rightSpaceToView(bottomContainer, 5)
  .centerYEqualToView(momAgeLabel)
  .heightIs(25)
  .widthIs(120);
  
  babyNameLabelR.sd_layout
  .rightSpaceToView(bottomContainer, 5)
  .centerYEqualToView(babyNameLabel)
  .heightIs(25)
  .widthIs(120);
  
  babyGenderLabelR.sd_layout
  .rightSpaceToView(bottomContainer, 5)
  .centerYEqualToView(babyGenderLabel)
  .heightIs(25)
  .widthIs(120);
  
  babyBirthLabelR.sd_layout
  .rightSpaceToView(bottomContainer, 5)
  .centerYEqualToView(babyBirthLabel)
  .heightIs(25)
  .widthIs(120);
  
  wechatLabelR.sd_layout
  .rightSpaceToView(bottomContainer, 5)
  .centerYEqualToView(wechatLabel)
  .heightIs(25)
  .widthIs(120);
  
  logoutBtn.sd_layout
  .rightSpaceToView(bottomContainer, 10)
  .leftSpaceToView(bottomContainer, 10)
  .topSpaceToView(wechatLabel, 20)
  .heightIs(30);
}

//退出登录
- (void)logoutClick {
//  [kUserDefaults objectForKey:@"UMCommunityLoginUid"];
  [kUserDefaults setObject:nil forKey:@"UMCommunityLoginUid"];
  
  [kUserDefaults setObject:nil forKey:@"NSUserDefaultSourceUID"];
  
  [kUserDefaults setObject:nil forKey:@"UMUserName"];
  
  [kUserDefaults setObject:nil forKey:@"UMUserIcon"];
  
  [self dismiss];

}

//创建标签视图
- (void)setupAutoWidthViewsWithCount:(NSInteger)count margin:(CGFloat)margin tagArr:(NSArray *)tagArr
{
  UIView *topContainer = [UIView new];
  self.topContainer = topContainer;
//  [self.view addSubview:topContainer];
  [self.view sd_addSubviews:@[topContainer]];
  
  NSMutableArray *temp = [NSMutableArray new];
  for (int i = 0; i < count; i++) {
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor lightGrayColor];
    label.text = tagArr[i];
    [topContainer addSubview:label];
    label.sd_layout.autoHeightRatio(0.4); // 设置高度约束
    [temp addObject:label];
  }
  
  UILabel *lastLabel = temp.lastObject;
  
  UIButton *addBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  addBtn.backgroundColor = [UIColor lightGrayColor];
  
  [addBtn setImage:[UIImage imageNamed:@"addTag"] forState:(UIControlStateNormal)];
  [addBtn addTarget:self action:@selector(addAction) forControlEvents:(UIControlEventTouchUpInside)];
  
  
  topContainer.sd_layout
  .leftSpaceToView(self.view, 30)
  .rightSpaceToView(self.view, 30)
  .topSpaceToView(self.nameLabel, 10);
  
    [topContainer sd_addSubviews:@[addBtn]];
  
  addBtn.sd_layout
  .heightRatioToView(lastLabel, 1)
  .centerYEqualToView(lastLabel)
  .leftSpaceToView(lastLabel, 10)
  .widthIs(30);
  
  
  // 此步设置之后_autoWidthViewsContainer的高度可以根据子view自适应
  [topContainer setupAutoWidthFlowItems:[temp copy] withPerRowItemsCount:4 verticalMargin:margin horizontalMargin:margin verticalEdgeInset:5 horizontalEdgeInset:10];
  
}


//修改个人信息方法
- (void)editClick {
  NSLog(@"%s", __func__);
}

//改变头像方法
- (void)imageTap {
  
  //直接弹出相册
  //创建相册
  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
  picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
  picker.delegate = self;
  picker.allowsEditing = YES;
  [self presentViewController:picker animated:YES completion:nil];
  
  //选择相机或者相册
//  NiceAlertSheet *alertSheet = [[NiceAlertSheet alloc] initWithMessage:nil choiceButtonTitles:@[@"相册", @"相机"]];
//  [alertSheet show];
//  alertSheet.choiceButtonClickedBlock = ^(NSInteger i) {
//    switch (i) {
//      case 0:
//      {
//        //创建相册
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        picker.delegate = self;
//        picker.allowsEditing = YES;
//        [self presentViewController:picker animated:YES completion:nil];
//      }
//        break;
//      case 1:
//      {
//        //判断是否设备存在相机,如果存在,弹出相机,如果不存在,告知用户没有相机
//        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//          // 创建相机
//          UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//          picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//          // 指定代理
//          picker.delegate = self;
//          // 允许编辑
//          picker.allowsEditing = YES;
//          [self presentViewController:picker animated:YES completion:nil];
//        }
//        else {
//          UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"没有相机" message:nil preferredStyle:UIAlertControllerStyleAlert];
//          UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
//          [controller addAction:action];
//          [self presentViewController:controller animated:YES completion:nil];
//        }
//
//      }
//        break;
//      default:
//        break;
//    }
//  };
  
}

//照片选择器的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
  UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];

  //压缩图片
  NSData * imageData = UIImagePNGRepresentation(image);
  float imagelength = imageData.length/1000;
  if (imagelength>500) {
    NSUInteger per = imagelength/500;
    if (per>=10) {
      per=10;
    }
    
    float perFloat = 1/(per*1.0);
    imageData = UIImageJPEGRepresentation(image, perFloat);
    
    [kUserDefaults setObject:imageData forKey:@"UMUserIcon"];
    
    self.headView.image = [UIImage imageWithData:imageData];
    
//    UMComUserAccount *userProfile = [[UMComUserAccount alloc] init];
//    userProfile.usid = @"lala";
//    userProfile.name = @"hehe";
    
    [UMComPushRequest updateUserIcon:self.headView.image response:^(NSError *error) {
      if (error) {
        NSLog(@"%@", error);
      }
    }];
    
    //选择相机或者相册
//   [UMComPushRequest loginWithCustomAccountForUser:userProfile completion:^(id responseObject, NSError *error) {
//    
//     NSLog(@"%@", responseObject);
//     
//    
//   }];
}
  
  //移除照片选择器
  [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismiss {
  [super dismiss];
  [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshImageAndName" object:nil];
}

//添加标签方法
- (void)addAction {
  UIView *addTagView = [[UIView alloc] init];
  self.addTagView = addTagView;
  addTagView.center = self.view.center;
  [addTagView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeAddTagView)]];
  [self.view addSubview:addTagView];
  addTagView.backgroundColor = [UIColor blackColor];
  addTagView.alpha = 0;
  addTagView.sd_layout
  .leftSpaceToView(self.view, kScreenWidth * 0.1)
  .rightSpaceToView(self.view, kScreenWidth * 0.1)
  .topSpaceToView(self.view, kScreenHeight * 0.1)
  .bottomSpaceToView(self.view, kScreenHeight * 0.15);
  
  UIView *selectView = [[UIView alloc] initWithFrame:CGRectZero];
  selectView.center = self.view.center;
  
  [self.view addSubview:selectView];
  selectView.backgroundColor = [UIColor whiteColor];
  self.selectView = selectView;
  [selectView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeAddTagView)]];
  
  [UIView animateWithDuration:0.3 animations:^{
    selectView.sd_layout
    .leftSpaceToView(self.view, kScreenWidth * 0.1)
    .rightSpaceToView(self.view, kScreenWidth * 0.1)
    .topSpaceToView(self.view, kScreenHeight * 0.1)
    .bottomSpaceToView(self.view, kScreenHeight * 0.15);
    
    [selectView updateLayout];
  } completion:^(BOOL finished) {
    addTagView.alpha = 0.8;
    [UIView animateWithDuration:0.1 animations:^{
      
      addTagView.sd_layout
      .leftSpaceToView(self.view, 0)
      .rightSpaceToView(self.view, 0)
      .topSpaceToView(self.view, 0)
      .bottomSpaceToView(self.view, 0);
      [addTagView updateLayout];
      
    } completion:nil];
  }];
  
  
}

- (void)removeAddTagView {
  [self.addTagView removeFromSuperview];
  [self.selectView removeFromSuperview];
}


@end
