//
//  RegistViewController.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/19.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "RegistViewController.h"
#import <SMS_SDK/SMSSDK.h>

@interface RegistViewController ()

@end

@implementation RegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.title = @"注册";
    // Do any additional setup after loading the view from its nib.
}

//发送短信
- (IBAction)smsClick:(id)sender {
  
  
  [SMSSDK getVerificationCodeByMethod:SMSGetCodeMethodSMS phoneNumber:self.phoneTF.text zone:@"86" customIdentifier:nil result:^(NSError *error) {
    
    if (!error) {
      NSLog(@"获取验证码成功");
      [(UIButton *)sender setTitle:@"已发送" forState:(UIControlStateNormal)];
    }else {
      NSLog(@"错误信息%@", error);
    }
  }];
  
}

//确认注册并且登录
- (IBAction)confirmClick:(id)sender {
  [SMSSDK commitVerificationCode:self.smsTF.text phoneNumber:self.phoneTF.text zone:@"86" result:^(NSError *error) {
    
    if (!error) {
      
      NSLog(@"验证成功");
      
      //友盟注册
      [UMComPushRequest userSignUpUMCommunity:self.countTF.text
                                     password:(NSString *)self.passwordTF.text
                                     nickName:(NSString *)self.nameTF.text
                                     response:^(id responseObject, NSError *error) {
                                       
                                       if (error) {
                                         NSLog(@"注册失败%@", error);
                                       }else {
                                         
                                         [UMComPushRequest userLoginInUMCommunity:self.countTF.text password:self.passwordTF.text response:^(id responseObject, NSError *error) {
                                           NSLog(@"登录成功%@", responseObject);
                                           
                                           
                                           //登陆后将登录信息存本地字典中。
//                                           [kUserDefaults setValue:<#(nullable id)#> forKey:<#(nonnull NSString *)#>]
                                           
                                           //注册成功后退出注册登录界面
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                             [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                           });
                                         }];
                                        
                                       }
                                     }];
      
    }
    else
    {
      NSLog(@"错误信息：%@",error);
      
    }
  }];
  
}


@end
