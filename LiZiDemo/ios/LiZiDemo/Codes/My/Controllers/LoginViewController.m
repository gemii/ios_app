//
//  LoginViewController.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/18.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "LoginViewController.h"
#import "RegistViewController.h"
#import <UMSocialWechatHandler.h>
#import <UMSocialSnsPlatformManager.h>
#import <UMSocialAccountManager.h>


@interface LoginViewController ()

@property (nonatomic, weak) UITextField *nameTF;

@property (nonatomic, weak) UITextField *passTF;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
  self.title = @"登陆";
  
  self.view.backgroundColor = [UIColor whiteColor];
  
  UIBarButtonItem *dismissItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dismiss"] style:(UIBarButtonItemStylePlain) target:self action:@selector(dismissLogin)];
  self.navigationItem.leftBarButtonItem = dismissItem;
  
  //创建控件
  UIImageView *userImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"userName"]];
  UIImageView *passwordImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
  UITextField *nameTF = [[UITextField alloc] init];
  UITextField *passwordTF = [[UITextField alloc] init];
  UIButton *loginBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  UIButton *registBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  UILabel *forgotLabel = [[UILabel alloc] init];
  forgotLabel.userInteractionEnabled = YES;
  forgotLabel.text = @"忘记密码";
  [forgotLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgotTap)]];
  forgotLabel.textAlignment = NSTextAlignmentRight;
  forgotLabel.textColor = [UIColor blueColor];
  
  //设置控件属性
  self.nameTF = nameTF;
  self.passTF = passwordTF;
  nameTF.placeholder = @"请输入用户名";
  passwordTF.placeholder = @"请输入密码";
  nameTF.spellCheckingType = UITextSpellCheckingTypeNo;
  nameTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
  passwordTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
  
  passwordTF.secureTextEntry = YES;
  
  
  [loginBtn addTarget:self action:@selector(loginAction) forControlEvents:(UIControlEventTouchUpInside)];
  [loginBtn setTitle:@"登陆" forState:(UIControlStateNormal)];
  loginBtn.layer.cornerRadius = 17;
  loginBtn.backgroundColor = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1];
  [loginBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
  
  
  [registBtn addTarget:self action:@selector(registAction) forControlEvents:(UIControlEventTouchUpInside)];
  [registBtn setTitle:@"注册" forState:(UIControlStateNormal)];
  registBtn.layer.cornerRadius = 17;
  registBtn.layer.borderWidth = 1;
  [registBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
  [registBtn.layer setBorderWidth:1.0]; //边框宽度
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  
  CGColorRef colorref = CGColorCreate(colorSpace,(CGFloat[]){ 0.4, 0.4, 0.4, 1 });
  
  [registBtn.layer setBorderColor:colorref];//边框颜色
  
  UIView *nameView = [[UIView alloc] init];
  UIView *passwordView = [[UIView alloc] init];
  
  
  //微信登陆按钮
  UIButton *vxBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  [vxBtn setImage:[UIImage imageNamed:@"vxLogin"] forState:(UIControlStateNormal)];
  [vxBtn addTarget:self action:@selector(vxLogin) forControlEvents:(UIControlEventTouchUpInside)];
  
  [self.view sd_addSubviews:@[nameView, passwordView, forgotLabel, loginBtn, registBtn, vxBtn]];
  
  nameView.sd_layout
  .leftSpaceToView(self.view, 0)
  .rightSpaceToView(self.view, 0)
  .topSpaceToView(self.view, 74)
  .heightIs(44);
  
  passwordView.sd_layout
  .leftSpaceToView(self.view, 0)
  .rightSpaceToView(self.view, 0)
  .topSpaceToView(nameView, 0)
  .heightIs(44);
  
  [nameView sd_addSubviews:@[userImage, nameTF]];
  [passwordView sd_addSubviews:@[passwordImage, passwordTF]];
  
  userImage.sd_layout
  .leftSpaceToView(nameView, 15)
  .topSpaceToView(nameView, 10)
  .bottomSpaceToView(nameView, 10)
  .widthIs(24);
  
  nameTF.sd_layout
  .leftSpaceToView(userImage, 15)
  .topSpaceToView(nameView, 10)
  .bottomSpaceToView(nameView, 10)
  .rightSpaceToView(nameView, 10);
  
  passwordImage.sd_layout
  .leftSpaceToView(passwordView, 15)
  .topSpaceToView(passwordView, 10)
  .bottomSpaceToView(passwordView, 10)
  .widthIs(24);
  
  passwordTF.sd_layout
  .leftSpaceToView(passwordImage, 15)
  .topSpaceToView(passwordView, 10)
  .bottomSpaceToView(passwordView, 10)
  .rightSpaceToView(passwordView, 10);
  
  forgotLabel.sd_layout
  .topSpaceToView(passwordView, 15)
  .rightSpaceToView(self.view, kScreenWidth * 0.05)
  .heightIs(20)
  .widthIs(100);
  
  loginBtn.sd_layout
  .leftSpaceToView(self.view, kScreenWidth * 0.1)
  .rightSpaceToView(self.view, kScreenWidth * 0.1)
  .topSpaceToView(forgotLabel, 30)
  .heightIs(34);
  
  registBtn.sd_layout
  .leftSpaceToView(self.view, kScreenWidth * 0.1)
  .rightSpaceToView(self.view, kScreenWidth * 0.1)
  .topSpaceToView(loginBtn, 15)
  .heightIs(34);
  
  vxBtn.sd_layout
  .centerXEqualToView(self.view)
  .widthIs(100)
  .heightIs(100)
  .topSpaceToView(registBtn, 50);
}

- (void)vxLogin {
  
   [UMSocialWechatHandler setWXAppId:@"wx98624676a082b5df" appSecret:@"de125f0c48b3a591bb20e96fe52eb624" url:@"https://itunes.apple.com/cn/app/dao-shu-ri-air-days-matter-air/id1113365292?mt=8"];
  
  UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
  
  snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
    
    if (response.responseCode == UMSResponseCodeSuccess) {
      
      NSDictionary *dict = [UMSocialAccountManager socialAccountDictionary];
      UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:snsPlatform.platformName];
//      NSLog(@"\nusername = %@,\n usid = %@,\n token = %@ iconUrl = %@,\n unionId = %@,\n thirdPlatformUserProfile = %@,\n thirdPlatformResponse = %@ \n, message = %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL, snsAccount.unionId, response.thirdPlatformUserProfile, response.thirdPlatformResponse, response.message);
      NSLog(@"snsAccount----%@", snsAccount);
      NSLog(@"字典----%@", dict);
      NSLog(@"登陆返回结果---%@", response);
      
    }
    
  });
}

//忘记密码
- (void)forgotTap {
  [UMComPushRequest userPasswordForgetForUMCommunity:self.nameTF.text response:^(id responseObject, NSError *error) {
    NSLog(@"%@", responseObject);
  }];
}

//注册按钮
- (void)registAction {
  RegistViewController *registVC = [[RegistViewController alloc] init];
  [self.navigationController pushViewController:registVC animated:YES];
}
//登陆按钮
- (void)loginAction {
  [UMComPushRequest userLoginInUMCommunity:self.nameTF.text password:self.passTF.text response:^(id responseObject, NSError *error) {
    if (!error) {
      NSLog(@"----%@", responseObject);
      
      NSLog(@"%@", [responseObject name]);
      
      [kUserDefaults setObject:[responseObject name] forKey:@"UMUserName"];
      
      
      [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }else {
      NSLog(@"登陆失败：%@", error);
    }
  }];
}
//返回按钮
- (void)dismissLogin {
  [self.navigationController dismissViewControllerAnimated:YES completion:nil];
  [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshImageAndName" object:nil];
}


@end
