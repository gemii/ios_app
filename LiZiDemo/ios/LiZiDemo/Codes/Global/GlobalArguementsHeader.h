//
//  GlobalArguementsHeader.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/17.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#ifndef GlobalArguementsHeader_h
#define GlobalArguementsHeader_h

#define kScreenWidth [UIScreen mainScreen].bounds.size.width

#define kScreenHeight [UIScreen mainScreen].bounds.size.height

#define kUserDefaults [NSUserDefaults standardUserDefaults]



#endif /* GlobalArguementsHeader_h */
