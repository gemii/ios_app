//
//  ASViewController.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/17.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "ASViewController.h"

@interface ASViewController ()

@end

@implementation ASViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   UIButton *dismissBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  
  [self.view sd_addSubviews:@[dismissBtn]];
  
  dismissBtn.sd_layout
  .rightSpaceToView(self.view, 0)
  .topSpaceToView(self.view, 0)
  .heightIs(20)
  .widthIs(20);
  
  [dismissBtn setBackgroundImage:[UIImage imageNamed:@"dismiss"] forState:(UIControlStateNormal)];
  [dismissBtn addTarget:self action:@selector(dismiss) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void)dismiss {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
