//
//  WeightModel.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/9/1.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeightModel : NSObject

@property (nonatomic, assign) CGFloat weeks;

@property (nonatomic, assign) CGFloat weight;



@end
