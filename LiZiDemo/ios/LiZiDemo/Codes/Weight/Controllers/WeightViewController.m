//
//  WeightViewController.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/9/1.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "WeightViewController.h"
#import "WeightLineScroll.h"
#import "DrawView.h"
#import "WeightModel.h"


@interface WeightViewController ()

@property (nonatomic, weak) UIScrollView *scrollerView;

@property (nonatomic, strong) NSMutableArray *weightArr;

@end

@implementation WeightViewController

- (NSMutableArray *)weightArr {
  if (_weightArr == nil) {
    _weightArr = @[].mutableCopy;
  }
  return _weightArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  self.view.backgroundColor = [UIColor whiteColor];
  
  [self createSubViews];
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showWeight:) name:@"showWeightInfo" object:nil];
}

- (void)showWeight:(NSNotification *)noti {
  UIView *showView = [[UIView alloc] init];
  showView.backgroundColor = [UIColor grayColor];
  showView.center = self.view.center;
  showView.size = CGSizeZero;
  [self.view addSubview:showView];
  UILabel *label = [[UILabel alloc] init];
  label.center = showView.center;
  label.size = CGSizeMake(150, 20);
  label.numberOfLines = 0;
  label.text = [NSString stringWithFormat:@"第%@周，体重：%@公斤", noti.object[@"weeks"], noti.object[@"weight"]];
  [showView addSubview:label];
  
  [UIView animateWithDuration:0.3 animations:^{
    showView.width = 200;
    showView.height = 200;
  } completion:nil];
  
}

- (void)createSubViews {
  
  
  
  CGFloat weight = 100;
  NSInteger kweight = (NSInteger)weight;

  
  for (int i = 1; i <= 40; i++) {
    WeightModel *model = [[WeightModel alloc] init];
//    model.weight = arc4random_uniform(20) + 50;
    CGFloat addS = arc4random()%4;
    CGFloat addX = arc4random()%5 + 1;
    weight += addS / addX;
    NSLog(@"----%f", weight);
    model.weight = weight;
    model.weeks = i;
    [self.weightArr addObject:model];
  }
  
  CGFloat maxWeight = 0;
  CGFloat minWeight = 100;
  
  for (WeightModel *model in self.weightArr) {
    if (maxWeight < model.weight) {
      maxWeight = model.weight;
    }
    if (minWeight > model.weight) {
      minWeight = model.weight;
    }
  }
  
  
  NSInteger xWeight = (NSInteger)(maxWeight - minWeight);
  
  
  
  WeightLineScroll *scrollView = [[WeightLineScroll alloc] init];
  self.scrollerView = scrollView;
  
  [self.view addSubview:scrollView];
  
  scrollView.sd_layout
  .leftSpaceToView(self.view, 50)
  .rightSpaceToView(self.view, 50)
  .topSpaceToView(self.view, 100)
  .heightIs(200);
  
  scrollView.contentSize = CGSizeMake(self.weightArr.count * 60, 200);
 
  scrollView.showsHorizontalScrollIndicator = NO;
  

  DrawView *drawView = [[DrawView alloc] initWithFrame:CGRectMake(0, 0, self.weightArr.count * 60, 200) WeightArr:self.weightArr MinWeight:minWeight MaxWeight:maxWeight XWeight:xWeight];
  
  drawView.backgroundColor = [UIColor clearColor];
  [scrollView addSubview:drawView];
  

  
  //添加y轴及横线
  for (int i = 0; i < 5; i++) {
    
    UILabel *yLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 300 - i * 40 , 50, 20)];
    yLabel.center = CGPointMake(20, 200 + 100 - 40 - i * (200 - 80) / 4);
    yLabel.text = [NSString stringWithFormat:@"%ld", kweight + xWeight / 4 * i];
    yLabel.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:yLabel];
    
    UIView *yLine = [[UIView alloc] init];
    yLine.x = 50;
    yLine.y = yLabel.center.y;
    yLine.height = 1;
    yLine.width = kScreenWidth - 100;
    yLine.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:yLine];
    
  }
  
}




@end
