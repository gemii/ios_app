//
//  DrawView.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/9/1.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "DrawView.h"

@implementation DrawView {
  CGFloat add;
  NSTimer *addTimer;
}


- (instancetype)initWithFrame:(CGRect)frame WeightArr:(NSMutableArray *)weightArr MinWeight:(CGFloat)minWeight MaxWeight:(CGFloat)maxWeight XWeight:(CGFloat)xWeight {
  self = [super initWithFrame:frame];
  if (self) {
    
    self.weightArr = [weightArr copy];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGFloat viewHeight = frame.size.height;
    
//    for (int i = 0; i <= 5; i++) {
//      
//      UIView *yLine = [[UIView alloc] init];
//      yLine.x = 0;
//      yLine.y = (viewHeight - 80)/ 5 * i;
//      yLine.width = self.width;
//      yLine.height = 1;
//      yLine.backgroundColor = [UIColor lightGrayColor];
//      
//      [self addSubview:yLine];
//      
//    }
    
    
    for (int i = 0; i < weightArr.count; i++) {
      
      UIView *xLine = [[UIView alloc] init];
      xLine.x = 40 + 60 * i;
      xLine.y = 0;
      xLine.width = 1;
      xLine.height = viewHeight - 25;
      xLine.backgroundColor = [UIColor lightGrayColor];
      [self addSubview:xLine];
      
      //创建体重按钮
      UIButton *button = [UIButton buttonWithType:(UIButtonTypeCustom)];
      button.tag = 100 + i;
      button.size = CGSizeMake(10, 10);
      button.layer.cornerRadius = 5;
      button.clipsToBounds = YES;
      
      //由于Y值由上至下是递减的，所以point的y值要反过来。
      CGPoint point = CGPointMake(40 + 60 * i, viewHeight - (40 + ([weightArr[i] weight] - minWeight) * ((viewHeight - 80) / xWeight)));
      
      button.center = point;
      
      UILabel *xLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, viewHeight - 20, 60, 20)];
      xLabel.center = CGPointMake(40 + 60 * i, viewHeight - 10);
      xLabel.text = [NSString stringWithFormat:@"第%d周", i + 1];
      xLabel.textAlignment = NSTextAlignmentCenter;
      xLabel.font = [UIFont systemFontOfSize:13];
      xLabel.textColor = [UIColor grayColor];
      [self addSubview:xLabel];      if (i == 0) {
      //设置初始值

        [path moveToPoint:point];
      }
      
      [button addTarget:self action:@selector(weightClick:) forControlEvents:(UIControlEventTouchUpInside)];
      [self addSubview:button];

      button.backgroundColor = [UIColor blackColor];
      [path addLineToPoint:point];
    }
    
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.frame = CGRectMake(0, 0, (kScreenWidth - 40) * 3, 400);
    self.shapeLayer = shapeLayer;
    //  self.shapeLayer.position = self.center;
    
    self.shapeLayer.lineWidth = 2;
    self.shapeLayer.strokeColor = [UIColor blueColor].CGColor;
    
    self.shapeLayer.fillColor = [UIColor clearColor].CGColor;//填充颜色为ClearColor
    
    shapeLayer.path = path.CGPath;
    
    add = 0;
    shapeLayer.strokeEnd = add;
    
    [self.layer addSublayer:shapeLayer];
  }
  
  addTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(addLine) userInfo:nil repeats:YES];
  //防止在滑动过程中定时器不走，将定时器加入runloop的common模式
  
  [[NSRunLoop currentRunLoop] addTimer:addTimer forMode:NSRunLoopCommonModes];
  
  return self;
}

- (void)weightClick:(UIButton *)btn {
  
  CGFloat weight = [self.weightArr[btn.tag - 100] weight];
  CGFloat weeks = [self.weightArr[btn.tag - 100] weight];
  
  NSLog(@"%f", [self.weightArr[btn.tag - 100] weight]);
  
  [[NSNotificationCenter defaultCenter] postNotificationName:@"showWeightInfo" object:@{@"weight":@(weight), @"weeks":@(weeks)}];
  
}


- (void)addLine {
  if (add >= 1) {
    [addTimer invalidate];
    addTimer = nil;
    return;
  }
  add += 0.02;
  self.shapeLayer.strokeEnd = add;
}

- (void)dealloc {
  if (addTimer != nil) {
    [addTimer invalidate];
    addTimer = nil;
  }
}

@end
