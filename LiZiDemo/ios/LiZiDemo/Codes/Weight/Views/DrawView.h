//
//  DrawView.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/9/1.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeightModel.h"

@interface DrawView : UIView

@property (nonatomic, strong) CAShapeLayer *shapeLayer;

- (instancetype)initWithFrame:(CGRect)frame WeightArr:(NSMutableArray *)weightArr MinWeight:(CGFloat)minWeight MaxWeight:(CGFloat)maxWeight XWeight:(CGFloat)vWeight;

@property (nonatomic, copy) NSArray *weightArr;




@end
