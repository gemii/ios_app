//
//  SocialModel.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocialModel : NSObject

@property (nonatomic, copy) NSString *context;

@property (nonatomic, copy) NSString *picUrl;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *iconUrl;

@property (nonatomic, copy) NSString *timeStr;

@end
