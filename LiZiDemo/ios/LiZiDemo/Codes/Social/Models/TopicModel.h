//
//  TopicModel.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TopicModel : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *picUrl;

@property (nonatomic, copy) NSString *topicId;

@end
