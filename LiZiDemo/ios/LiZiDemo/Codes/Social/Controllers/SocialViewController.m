//
//  SocialViewController.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/17.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "SocialViewController.h"
#import "SocialModel.h"
#import "SocialListTableViewCell.h"
@interface SocialViewController ()<UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) UITableView *tabelView;

@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, weak) UITextView *textView;

@property (nonatomic, weak) UIView *containerView;

@property (nonatomic, weak) UIImageView *imageView;

@property (nonatomic, weak) UIView *unclearView;

@end

@implementation SocialViewController

//懒加载
- (NSMutableArray *)dataArr {
  if (_dataArr == nil) {
    _dataArr = @[].mutableCopy;
  }
  return _dataArr;
}

- (UITableView *)tabelView {
  if (_tabelView == nil) {
    UITableView * tableView= [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64 - 50) style:(UITableViewStylePlain)];
    _tabelView = tableView;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
  }
  return _tabelView;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.view.backgroundColor = [UIColor whiteColor];
  
  self.automaticallyAdjustsScrollViewInsets = NO;
  
  self.title = self.topicName;
  
  [self setSubViews];
  
  [self getData];
  
}

- (void)setSubViews {
  
  [self.tabelView registerNib:[UINib nibWithNibName:@"SocialListTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"socialListCell"];
  
  UIView *containerView = [[UIView alloc] init];
  self.containerView = containerView;
  
  UIImageView *picImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"选取照片"]];
  picImage.userInteractionEnabled = YES;
  [picImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choosePic)]];
  self.imageView = picImage;
  
  UITextView *textView = [[UITextView alloc] init];
  textView.backgroundColor = [UIColor grayColor];
  textView.delegate = self;
  self.textView = textView;
  
  UIImageView *sendImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"发送"]];
  sendImage.userInteractionEnabled = YES;
  
  [self.view addSubview:containerView];
  
  containerView.sd_layout
  .leftSpaceToView(self.view, 0)
  .rightSpaceToView(self.view, 0)
  .bottomSpaceToView(self.view, 0)
  .heightIs(50);
  
  [containerView sd_addSubviews:@[picImage, textView, sendImage]];
  
  picImage.sd_layout
  .leftSpaceToView(containerView, 10)
  .topSpaceToView(containerView, 10)
  .bottomSpaceToView(containerView, 10)
  .widthIs(30);
  
  sendImage.sd_layout
  .rightSpaceToView(containerView, 10)
  .topSpaceToView(containerView, 10)
  .bottomSpaceToView(containerView, 10)
  .widthIs(30);
  
  textView.sd_layout
  .leftSpaceToView(picImage, 10)
  .topSpaceToView(containerView, 10)
  .bottomSpaceToView(containerView, 10)
  .rightSpaceToView(sendImage, 10);
  
  
}

//选取照片
- (void)choosePic {
  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
  picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
  picker.delegate = self;
  picker.allowsEditing = YES;
  [self presentViewController:picker animated:YES completion:nil];
}

//发送
- (void)sendAction {
  
  
}

//获取数据
- (void)getData {
  
//  AVQuery *query = [AVQuery queryWithClassName:@"LiZiFeed"];
  
  for (int i = 0; i < 20; i++) {
    SocialModel *model = [SocialModel new];
    model.context = [NSString stringWithFormat:@"这是第%d个%@",i, self.topicName];
    model.name = [NSString stringWithFormat:@"第%d个用户", i];
    model.iconUrl = [NSString stringWithFormat:@"%d", i % 7 + 1];
    
    NSDate *date = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd HH:mm"];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    
    model.timeStr = dateStr;
    
    [self.dataArr addObject:model];
  }
  
  [self.tabelView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  SocialListTableViewCell *cell = [self.tabelView dequeueReusableCellWithIdentifier:@"socialListCell" forIndexPath:indexPath];
  
  SocialModel *model = self.dataArr[indexPath.row];
  
  [cell setContentWith:model];
  
  
  return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.dataArr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 250;
}

@end
