//
//  TopicListViewController.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "TopicListViewController.h"
#import "TopicModel.h"
#import "CollectionViewCell.h"
#import "SocialViewController.h"

@interface TopicListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *topicCollectionView;

@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation TopicListViewController

- (NSMutableArray *)dataArr {
  if (_dataArr == nil) {
    _dataArr = @[].mutableCopy;
  }
  return _dataArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
  self.view.backgroundColor = [UIColor whiteColor];
  
  
  [self setSubViews];
  
  [self getData];
  
}

- (void)getData {
  TopicModel *topicModel1 = [[TopicModel alloc] init];
  TopicModel *topicModel2 = [[TopicModel alloc] init];
  TopicModel *topicModel3 = [[TopicModel alloc] init];
  TopicModel *topicModel4 = [[TopicModel alloc] init];
  
  topicModel1.name = @"lalala";
  topicModel2.name = @"hahaha";
  topicModel3.name = @"yeyeye";
  topicModel4.name = @"hehehe";
  
  topicModel1.topicId = @"topic1";
  topicModel2.topicId = @"topic2";
  topicModel3.topicId = @"topic3";
  topicModel4.topicId = @"topic4";
  
  [self.dataArr addObject:topicModel1];
  [self.dataArr addObject:topicModel2];
  [self.dataArr addObject:topicModel3];
  [self.dataArr addObject:topicModel4];
  
  [self.topicCollectionView reloadData];
}

//创建子控件
- (void)setSubViews {
  

  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  // 最小行间距
  flowLayout.minimumLineSpacing = 5;
  flowLayout.itemSize = CGSizeMake((kScreenWidth - 10) / 2, (kScreenWidth - 10) / 2);
  
  // 最小的item间距
  flowLayout.minimumInteritemSpacing = 0;
  UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, kScreenHeight - 64) collectionViewLayout:flowLayout];
  
  self.topicCollectionView = collectionView;
  collectionView.backgroundColor = [UIColor whiteColor];
  [self.view addSubview:self.topicCollectionView];
  self.topicCollectionView.delegate = self;
  self.topicCollectionView.dataSource = self;
  [self.topicCollectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"topicCell"];
  
  //设置退出社区按钮
  UIBarButtonItem *dismissItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dismiss"] style:(UIBarButtonItemStylePlain) target:self action:@selector(dismissAction)];
  self.navigationItem.leftBarButtonItem = dismissItem;
 
  self.title = @"话题";
  
}


- (void)dismissAction {
  [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
  CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"topicCell" forIndexPath:indexPath];
  
  [cell setcontentWith:self.dataArr[indexPath.row]];
  
  return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return self.dataArr.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  
  TopicModel *model = self.dataArr[indexPath.row];
  SocialViewController *socialVC = [SocialViewController new];
  socialVC.topicId = model.topicId;
  socialVC.topicName = model.name;
  
  [self.navigationController pushViewController:socialVC animated:YES];
  
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [[UIApplication sharedApplication] setStatusBarHidden:NO];
  
}

@end
