//
//  SocialViewController.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/17.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialViewController : UIViewController

@property (nonatomic, copy) NSString *topicId;

@property (nonatomic, copy) NSString *topicName;

@end
