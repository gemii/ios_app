//
//  SocialListTableViewCell.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialModel.h"

@interface SocialListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headView;


@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *contentImg;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

- (void)setContentWith:(SocialModel *)model;

@end
