//
//  SocialListTableViewCell.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "SocialListTableViewCell.h"

@implementation SocialListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setContentWith:(SocialModel *)model {
  
  self.nameLabel.text = model.name;
  self.headView.image = [UIImage imageNamed:model.iconUrl];
  self.contentLabel.text = model.context;
  self.timeLabel.text = model.timeStr;
  
  [self.contentImg sd_setImageWithURL:[NSURL URLWithString:model.picUrl] placeholderImage:[UIImage imageNamed:@"game"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    
  }];
  
  
  
}

@end
