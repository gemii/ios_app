//
//  CollectionViewCell.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopicModel.h"
@interface CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *topicImg;

@property (weak, nonatomic) IBOutlet UILabel *topicName;

- (void)setcontentWith:(TopicModel *)topicModel;

@end
