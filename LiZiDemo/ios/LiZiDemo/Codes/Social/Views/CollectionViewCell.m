//
//  CollectionViewCell.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
}

- (void)setcontentWith:(TopicModel *)topicModel {
  
  self.topicName.text = topicModel.name;
  
  [self.topicImg sd_setImageWithURL:[NSURL URLWithString:topicModel.picUrl] placeholderImage:[UIImage imageNamed:@"game"]];
  
  
}

@end
