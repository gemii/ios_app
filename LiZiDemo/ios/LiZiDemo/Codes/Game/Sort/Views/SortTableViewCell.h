//
//  SortTableViewCell.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/29.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortModel.h"

@interface SortTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)setContentWithModel:(SortModel *)model;

@end
