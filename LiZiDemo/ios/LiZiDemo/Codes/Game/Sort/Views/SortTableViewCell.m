//
//  SortTableViewCell.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/29.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "SortTableViewCell.h"

@implementation SortTableViewCell

- (void)setContentWithModel:(SortModel *)model{
  
  self.titleLabel.text = model.title;
  
}

- (void)setFrame:(CGRect)frame {
  
  frame = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(10, 30, 10, 30));
  [super setFrame:frame];
  
  
}

@end
