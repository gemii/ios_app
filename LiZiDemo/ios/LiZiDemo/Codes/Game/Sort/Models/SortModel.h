//
//  SortModel.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/29.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SortModel : NSObject

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *ID;

@end
