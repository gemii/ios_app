//
//  SortButton.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/30.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "SortButton.h"

@implementation SortButton


+ (instancetype)buttonWithType:(UIButtonType)buttonType Title:(NSString *)title ID:(NSString *)ID {
  SortButton *sortBtn = [super buttonWithType:buttonType];
  if (sortBtn) {
    sortBtn.title = title;
    sortBtn.ID = ID;
  }
  return sortBtn;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
