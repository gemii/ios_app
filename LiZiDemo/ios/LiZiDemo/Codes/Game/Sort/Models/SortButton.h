//
//  SortButton.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/30.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SortButton : UIButton

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *ID;

+ (instancetype)buttonWithType:(UIButtonType)buttonType Title:(NSString *)title ID:(NSString *)ID;

@end
