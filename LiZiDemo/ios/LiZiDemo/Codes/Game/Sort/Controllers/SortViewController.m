//
//  SortViewController.m
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/29.
//  Copyright © 2016年 Facebook. All rights reserved.
//


#import "SortViewController.h"
#import "SortModel.h"
#import "SortTableViewCell.h"
#import "SortButton.h"

#define KBase_tag 100

@interface SortViewController ()

//@property (nonatomic, strong) NSMutableArray *dataArr;

@property (nonatomic, copy) NSString *answer;

@end

@implementation SortViewController{
  
  // 开始拖动的view的下一个view的CGPoint（如果开始位置是0 结束位置是4 nextPoint值逐个往下算）
  CGPoint nextPoint;
  
  // 用于赋值CGPoint
  CGPoint valuePoint;
  
}

//懒加载数组
//- (NSMutableArray *)dataArr {
//  if (_dataArr == nil) {
//    _dataArr = @[].mutableCopy;
//  }
//  return _dataArr;
//}

- (void)viewDidLoad {
  
    [super viewDidLoad];
  
  self.view.backgroundColor = [UIColor whiteColor];
  
  self.answer = @"012345687";
  
  [self createSubViews];
  
  
  
//  [self getData];
}

//- (void)getData {
//  for (int i = 0; i < 10; i++) {
//    SortModel *model = [[SortModel alloc] init];
//    
//    model.title = [NSString stringWithFormat:@"这个是第%d个", i];
//    model.ID = [NSString stringWithFormat:@"%d", i];
//    
//    
//    [self.dataArr addObject:model];
//  }
//}

- (void)createSubViews {
  
  //创建Button
  CGFloat btW = (kScreenWidth-40);
  CGFloat btH = 50;
  
  for (NSInteger i = 0; i<9; i++) {
    
    NSString *title = [NSString stringWithFormat:@"这是第%ld个", (long)i];
    NSString *ID = [NSString stringWithFormat:@"%ld", (long)i];
    
    SortButton * bt = [SortButton buttonWithType:(UIButtonTypeCustom) Title:title ID:ID];
    
    bt.frame = CGRectMake(20, 20 + i*(btH+20), btW, btH);
    bt.layer.cornerRadius = 5;
    bt.backgroundColor = [UIColor grayColor];
    bt.tag = KBase_tag+i;
    [bt setTitle:bt.title forState:UIControlStateNormal];
    
    [self.view addSubview:bt];
    
    // 长按手势
    UILongPressGestureRecognizer * longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    longPress.minimumPressDuration = 0.01;
    [bt addGestureRecognizer:longPress];
  }
}

//拖动手势
-(void)longPress:(UIGestureRecognizer*)recognizer{
  //
  UIButton *recognizerView = (UIButton *)recognizer.view;
  
  // 禁用其他按钮的拖拽手势
  for (UIButton * bt in self.view.subviews) {
    if (bt!=recognizerView) {
      bt.userInteractionEnabled = NO;
    }
  }
  
  // 长按视图在父视图中的位置（触摸点的位置）
  CGPoint recognizerPoint = [recognizer locationInView:self.view];
//  NSLog(@"_____%@",NSStringFromCGPoint(recognizerPoint));
  
  if (recognizer.state == UIGestureRecognizerStateBegan) {
    
    // 开始的时候改变拖动view的外观（放大，改变颜色等）
    [UIView animateWithDuration:0.2 animations:^{
      recognizerView.transform = CGAffineTransformMakeScale(1.3, 1.3);
      recognizerView.transform = CGAffineTransformRotate(recognizerView.transform, M_PI / 50);
      recognizerView.alpha = 0.7;
    }];
    
    // 把拖动view放到最上层
    [self.view bringSubviewToFront:recognizerView];
    
    // valuePoint保存最新的移动位置
    valuePoint = recognizerView.center;
    
  }else if(recognizer.state == UIGestureRecognizerStateChanged){
    
    // 更新pan.view的center
    recognizerView.center = recognizerPoint;
    
    /**
     * 可以创建一个继承UIButton的类(MyButton)，这样便于扩展，增加一些属性来绑定数据
     * 如果在self.view上加其他控件拖拽会奔溃，可以在下面方法里面加判断MyButton，也可以把所有按钮放到一个全局变量的UIView上来替换self.view
     
     */
    for (UIButton * bt in self.view.subviews) {
      
      // 判断是否移动到另一个view区域
      // CGRectContainsPoint(rect,point) 判断某个点是否被某个frame包含
      if (CGRectContainsPoint(bt.frame, recognizerView.center)&&bt!=recognizerView)
      {
//        NSLog(@"bt_______%@",bt);
        // 开始位置
        NSInteger fromIndex = recognizerView.tag - KBase_tag;
        
        // 需要移动到的位置
        NSInteger toIndex = bt.tag - KBase_tag;
//        NSLog(@"开始位置=%ld  结束位置=%ld",fromIndex,toIndex);
        
        // 往下移动
        if ((toIndex-fromIndex)>0) {
          
          // 从开始位置移动到结束位置
          // 把移动view的下一个view移动到记录的view的位置(valuePoint)，并把下一view的位置记为新的nextPoint，并把view的tag值-1,依次类推
          [UIView animateWithDuration:0.2 animations:^{
            for (NSInteger i = fromIndex+1; i<=toIndex; i++) {
              UIButton * nextBt = (UIButton*)[self.view viewWithTag:KBase_tag+i];
              nextPoint = nextBt.center;
              nextBt.center = valuePoint;
              valuePoint = nextPoint;
              
              nextBt.tag--;
            }
            recognizerView.tag = KBase_tag + toIndex;
          }];
          
        }
        // 往前移动
        else
        {
          // 从开始位置移动到结束位置
          // 把移动view的上一个view移动到记录的view的位置(valuePoint)，并把上一view的位置记为新的nextPoint，并把view的tag值+1,依次类推
          [UIView animateWithDuration:0.2 animations:^{
            for (NSInteger i = fromIndex-1; i>=toIndex; i--) {
              UIButton * nextBt = (UIButton*)[self.view viewWithTag:KBase_tag+i];
              nextPoint = nextBt.center;
              nextBt.center = valuePoint;
              valuePoint = nextPoint;
              
              nextBt.tag++;
            }
            recognizerView.tag = KBase_tag + toIndex;
            
          }];
        }
      }
      
    }
    
  }else if(recognizer.state == UIGestureRecognizerStateEnded){
    // 恢复其他按钮的拖拽手势
    for (UIButton * bt in self.view.subviews) {
      if (bt!=recognizerView) {
        bt.userInteractionEnabled = YES;
      }
    }
    
    // 结束时候恢复view的外观（放大，改变颜色等）
    [UIView animateWithDuration:0.2 animations:^{
      recognizerView.transform = CGAffineTransformMakeScale(1.0, 1.0);
      recognizerView.alpha = 1;
      
      recognizerView.center = valuePoint;
      NSMutableString *mutableStr = @"".mutableCopy;
      for (int i = 0; i < 9; i++) {
        SortButton *sortBtn = [self.view viewWithTag:KBase_tag + i];
        [mutableStr appendString:sortBtn.ID];
      }
      if ([self.answer isEqualToString:mutableStr]) {
        NSLog(@"你答对了");
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"答对了" message:@"" preferredStyle:(UIAlertControllerStyleAlert)];
        [alertVC addAction:[UIAlertAction actionWithTitle:@"好" style:(UIAlertActionStyleDefault) handler:nil]];
        [self presentViewController:alertVC animated:YES completion:nil];
        
      }
    }];
  }
}


@end
