//
//  LiZiGameViewController.h
//  LiZiDemo
//
//  Created by 刘伟 on 16/8/29.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASViewController.h"

@interface LiZiGameViewController : ASViewController

@property (nonatomic, copy) NSString *receive1;

@property (nonatomic, copy) NSString *receive2;

@end
