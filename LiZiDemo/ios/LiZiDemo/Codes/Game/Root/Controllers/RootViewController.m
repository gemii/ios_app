//
//  RootViewController.m
//  HelloWorld
//
//  Created by 刘伟 on 16/8/11.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "RootViewController.h"
#import "RCTBundleURLProvider.h"
#import "RCTRootView.h"

#import "RCTConvert.h"
#import "RCTBridge.h"
#import "RCTEventDispatcher.h"
#import "NativeViewController.h"
#import "LiZiGameViewController.h"
#import "SortViewController.h"


@interface RootViewController ()



@end

@implementation RootViewController

@synthesize bridge=_bridge;

//默认名称
RCT_EXPORT_MODULE()
//对外提供调用方法

RCT_EXPORT_METHOD(addEvent:(NSString *)number){
  //  NSLog(@"Pretending to create an event %@ at %@", name, location);
 
  dispatch_async(dispatch_get_main_queue(), ^{
   
    [[NSNotificationCenter defaultCenter] postNotificationName:@"push" object:number];
  });
  
}

RCT_EXPORT_METHOD(addEventDismiss){
  //  NSLog(@"Pretending to create an event %@ at %@", name, location);
  
  NSLog(@"dismiss");
  dispatch_async(dispatch_get_main_queue(), ^{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dismiss" object:nil];
  });
}

- (void)dismiss {
  NSLog(@"lalal");
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)pushVC:(NSNotification *)noti {
  
  NSString *gameid = noti.object;
  NSInteger ID = gameid.integerValue;
  
  LiZiGameViewController * nativeVC = nil;
  
  if (ID <= 7) {
    nativeVC = [NativeViewController new];
  }else if (ID <= 15) {
    nativeVC = [SortViewController new];
  }
  nativeVC.receive1 = gameid;
  
  [self presentViewController:nativeVC animated:YES completion:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushVC:) name:@"push" object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismiss) name:@"dismiss" object:nil];
  self.title = @"RN视图";
  
  NSURL *jsCodeLocation;
  
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"HelloWorld"
                                               initialProperties:nil
                                                   launchOptions:nil];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
  self.view = rootView;
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
