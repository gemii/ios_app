//
//  RootViewController.h
//  HelloWorld
//
//  Created by 刘伟 on 16/8/11.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCTBridgeModule.h"

@interface RootViewController : UIViewController<RCTBridgeModule>

@end
