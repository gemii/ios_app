//
//  GameViewController.m
//  HelloWorld
//
//  Created by 刘伟 on 16/8/12.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "GameViewController.h"
#import "RootViewController.h"
#import "MyViewController.h"
#import "SettingViewController.h"
#import "LZNAVC.h"
#import "TopicListViewController.h"
#import "WeightViewController.h"

@interface GameViewController ()

@property (nonatomic, weak) UILabel *nameLabel;

@property (nonatomic, weak) UIImageView *headView;

@end

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
  //设置背景色
    self.view.backgroundColor = [UIColor whiteColor];
  [self createSubViews];
  
//  NSOperationQueue *mainQueue = 
  [[NSNotificationCenter defaultCenter] addObserverForName:@"refreshImageAndName" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
    [self setNameAndImage];
  }];
  
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)createSubViews {
  
  UIButton *startBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  [startBtn addTarget:self action:@selector(startGame) forControlEvents:(UIControlEventTouchUpInside)];
  [startBtn setImage:[UIImage imageNamed:@"game"] forState:(UIControlStateNormal)];
  
  
  UIButton *socialBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  [socialBtn addTarget:self action:@selector(socialClick) forControlEvents:(UIControlEventTouchUpInside)];
  [socialBtn setImage:[UIImage imageNamed:@"social"] forState:(UIControlStateNormal)];
  
  UIButton *settingBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  [settingBtn addTarget:self action:@selector(settingClick) forControlEvents:(UIControlEventTouchUpInside)];
  [settingBtn setImage:[UIImage imageNamed:@"setting"] forState:(UIControlStateNormal)];
  
  UIButton *weightBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
  [weightBtn addTarget:self action:@selector(weightClick) forControlEvents:(UIControlEventTouchUpInside)];
  [weightBtn setImage:[UIImage imageNamed:@"weight"] forState:(UIControlStateNormal)];
  
  UIView *personView = [[UIView alloc] init];
  [personView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(personViewClick)]];
  
  [self.view sd_addSubviews:@[startBtn, socialBtn, settingBtn, personView,weightBtn]];
  
  personView.sd_layout
  .leftSpaceToView(self.view, 15)
  .topSpaceToView(self.view, 15)
  .widthIs(kScreenWidth * 0.4)
  .heightIs(kScreenWidth * 0.1);
  
  socialBtn.sd_layout
  .leftSpaceToView(self.view, kScreenWidth * 0.1)
  .bottomSpaceToView(self.view, kScreenWidth * 0.1)
  .widthIs(kScreenWidth * 0.12)
  .heightIs(kScreenWidth * 0.12);
  
  settingBtn.sd_layout
  .rightSpaceToView(self.view, kScreenWidth * 0.1)
  .bottomSpaceToView(self.view, kScreenWidth * 0.1)
  .widthIs(kScreenWidth * 0.12)
  .heightIs(kScreenWidth * 0.12);
  
  weightBtn.sd_layout
  .centerXEqualToView(self.view)
  .bottomSpaceToView(self.view, kScreenWidth * 0.1)
  .widthIs(kScreenWidth * 0.12)
  .heightIs(kScreenWidth * 0.12);
  
  startBtn.sd_layout
  .centerXIs(self.view.center.x)
  .centerYIs(self.view.center.y)
  .widthIs(kScreenWidth * 0.6)
  .heightIs(kScreenWidth * 0.6);
  
  //创建头像
  UIImageView *headView = [[UIImageView alloc] init];
  headView.backgroundColor = [UIColor whiteColor];
  headView.userInteractionEnabled = YES;
  headView.layer.cornerRadius = kScreenWidth * 0.05;
  headView.clipsToBounds = YES;
  self.headView = headView;
  
  //创建昵称
  UILabel *nameLabel = [[UILabel alloc] init];
  nameLabel.userInteractionEnabled = YES;
  self.nameLabel = nameLabel;
  
  [self setNameAndImage];
  
  
  [personView sd_addSubviews:@[headView, nameLabel]];
  
  headView.sd_layout
  .leftSpaceToView(personView, 0)
  .topSpaceToView(personView, 0)
  .bottomSpaceToView(personView, 0)
  .widthIs(kScreenWidth * 0.1);
  
  nameLabel.sd_layout
  .leftSpaceToView(headView, 5)
  .centerYEqualToView(headView)
  .rightSpaceToView(personView, 0)
  .heightIs(kScreenWidth * 0.06);
  
  
  
}

- (void)weightClick {
  WeightViewController *weightVC = [[WeightViewController alloc] init];
  [self presentViewController:weightVC animated:YES completion:nil];
}

//显示个人昵称与头像
- (void)setNameAndImage {
  
  if ([kUserDefaults objectForKey:@"UMUserName"] == nil) {
    self.nameLabel.text = @"用户名";
    self.headView.image = [UIImage imageNamed:@"头像"];
  }else {
    self.nameLabel.text = [kUserDefaults objectForKey:@"UMUserName"];
     NSData *imageData = [kUserDefaults objectForKey:@"UMUserIcon"];
    //加入为空则从服务器请求
    if (imageData == nil) {
      
      
      //不为空则从本地去获取
    }else {
       self.headView.image = [UIImage imageWithData:imageData];
    }
   
  }
  
}

//跳转至个人中心
- (void)personViewClick {
  
  
  if ([kUserDefaults objectForKey:@"NSUserDefaultSourceUID"]) {
    
    MyViewController *myVC = [[MyViewController alloc] init];
    [self presentViewController:myVC animated:YES completion:nil];
    
  } else {
    
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    
    UINavigationController *naVC = [[UINavigationController alloc] initWithRootViewController:loginVC];
    
    [self presentViewController:naVC animated:YES completion:nil];
    
    
  }
  
}

//跳转至设置
- (void)settingClick {
  SettingViewController *settingVC = [[SettingViewController alloc] init];
  [self presentViewController:settingVC animated:YES completion:nil];
}

//跳转至社区
- (void)socialClick {
  
  if ([kUserDefaults objectForKey:@"NSUserDefaultSourceUID"]) {
    
      TopicListViewController *topicVC = [[TopicListViewController alloc] init];
    
      LZNAVC *lzNaVC = [[LZNAVC alloc] initWithRootViewController:topicVC];
    
      [self presentViewController:lzNaVC animated:YES completion:nil];
    
  } else {
    
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    
    UINavigationController *naVC = [[UINavigationController alloc] initWithRootViewController:loginVC];
    
    [self presentViewController:naVC animated:YES completion:nil];
    
    
  }
  
}

//跳转至游戏菜单
- (void)startGame{
  
  if ([kUserDefaults objectForKey:@"NSUserDefaultSourceUID"]) {
    
    RootViewController *rootVC = [RootViewController new];
    [self presentViewController:rootVC animated:YES completion:nil];
    
  } else {
    
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    
    UINavigationController *naVC = [[UINavigationController alloc] initWithRootViewController:loginVC];
    
    [self presentViewController:naVC animated:YES completion:nil];
    
    
    
  }
  
}

- (void)viewWillAppear:(BOOL)animated {
  
  [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
  
  
}


@end
