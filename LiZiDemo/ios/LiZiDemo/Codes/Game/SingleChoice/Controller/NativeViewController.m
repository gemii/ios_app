//
//  NativeViewController.m
//  HelloWorld
//
//  Created by 刘伟 on 16/8/11.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "NativeViewController.h"
#import <AFNetworking.h>
#import "Question.h"

#define kUrl @"http://wx.gemii.cc/weixin/activity/getQuestions"

#define kWidth [UIScreen mainScreen].bounds.size.width
#define kHeight [UIScreen mainScreen].bounds.size.height

@interface NativeViewController ()

@property (nonatomic, weak) UIButton *btnA;

@property (nonatomic, weak) UIButton *btnB;

@property (nonatomic, weak) UIButton *btnC;

@property (nonatomic, weak) UIButton *btnD;

@property (nonatomic, weak) UILabel *labelQ;

@property (nonatomic, copy) NSArray *answerArr;

@property (nonatomic, strong) NSMutableArray *questionArr;

@property (nonatomic, assign) NSInteger questionIndex;

@end

@implementation NativeViewController

//懒加载问题数组
- (NSMutableArray *)questionArr {
  if (_questionArr == nil) {
    _questionArr = @[].mutableCopy;
  }
  return _questionArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
  self.questionIndex = 1;
  
  self.view.backgroundColor = [UIColor whiteColor];
  
  [self creatSubViews];
  
  [self getLocalData];
  
  self.answerArr = @[@"A", @"B", @"C", @"D"];
}

//创建子控件
- (void)creatSubViews {
  
  UILabel *labelQ = [UILabel new];
  UIButton *btnA = [UIButton buttonWithType:(UIButtonTypeCustom)];
  UIButton *btnB = [UIButton buttonWithType:(UIButtonTypeCustom)];
  UIButton *btnC = [UIButton buttonWithType:(UIButtonTypeCustom)];
  UIButton *btnD = [UIButton buttonWithType:(UIButtonTypeCustom)];
  
  self.labelQ = labelQ;
  self.btnA = btnA;
  self.btnB = btnB;
  self.btnC = btnC;
  self.btnD = btnD;
  
  [self.view sd_addSubviews:@[labelQ, btnA, btnB, btnC, btnD]];
  
  //设置所有子控件约束
  
  
  labelQ.sd_layout
  .leftSpaceToView(self.view, 20)
  .rightSpaceToView(self.view, 20)
  .topSpaceToView(self.view, 30)
  .heightIs(kHeight - 30 - kWidth);
  
  btnA.sd_layout
  .topSpaceToView(self.labelQ, 20)
  .leftSpaceToView(self.view, 20)
  .widthIs((kWidth - 50) / 2)
  .heightIs((kWidth - 50) / 2);
  
  btnB.sd_layout
  .topSpaceToView(self.labelQ, 20)
  .rightSpaceToView(self.view, 20)
  .widthIs((kWidth - 50) / 2)
  .heightIs((kWidth - 50) / 2);
  
  btnC.sd_layout
  .topSpaceToView(btnA, 10)
  .leftSpaceToView(self.view, 20)
  .widthIs((kWidth - 50) / 2)
  .heightIs((kWidth - 50) / 2);

  btnD.sd_layout
  .topSpaceToView(btnB, 10)
  .rightSpaceToView(self.view, 20)
  .widthIs((kWidth - 50) / 2)
  .heightIs((kWidth - 50) / 2);
  
  [btnA addTarget:self action:@selector(tap:) forControlEvents:(UIControlEventTouchUpInside)];
  [btnB addTarget:self action:@selector(tap:) forControlEvents:(UIControlEventTouchUpInside)];
  [btnC addTarget:self action:@selector(tap:) forControlEvents:(UIControlEventTouchUpInside)];
  [btnD addTarget:self action:@selector(tap:) forControlEvents:(UIControlEventTouchUpInside)];
  
  btnA.tag = 101;
  btnB.tag = 102;
  btnC.tag = 103;
  btnD.tag = 104;
  
  //选项颜色
  btnA.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1];
  btnB.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1];
  btnC.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1];
  btnD.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1];
  //题目颜色
  labelQ.backgroundColor = [UIColor lightGrayColor];
  
  //设置行数
  btnA.titleLabel.numberOfLines = 0;
  btnB.titleLabel.numberOfLines = 0;
  btnC.titleLabel.numberOfLines = 0;
  btnD.titleLabel.numberOfLines = 0;
  
  labelQ.numberOfLines = 0;
  
  
  
}

- (void)tap:(UIButton *)btn {
  
  NSString *answer = self.answerArr[btn.tag - 101];
  if ([answer isEqualToString:[self.questionArr[self.questionIndex] answer]]) {
    NSLog(@"你答对了");
    self.questionIndex++;
    if (self.questionIndex < self.questionArr.count) {
      [self refreshBtnTitleWithIndex:self.questionIndex];
    }else {
      [self dismissViewControllerAnimated:YES completion:nil];
    }
  }else {
    NSLog(@"错了");
    
  }
}

- (void)getLocalData {
  
  NSString *key = [NSString stringWithFormat:@"question%@", self.receive1];
  NSArray *questions = [[NSUserDefaults standardUserDefaults] objectForKey:key];
  if (questions.count > 0) {
    [self makeArrWith:questions Index:self.questionIndex];
  }else {
    [self requestWithNum:self.receive1];
  }
}
//解析数组方法
- (void)makeArrWith:(NSArray *)responseObject Index:(NSInteger)index {
  if ([responseObject count] > 0) {
    self.labelQ.text = responseObject[0][@"title"];
    
    for (int i = 0; i < [responseObject count]; i++) {
      
      Question *question = [[Question alloc] init];
      NSLog(@"%@", responseObject);
      [question setValuesForKeysWithDictionary:responseObject[i]];
      question.choiceA = responseObject[i][@"optionMap"][@"A"];
      question.choiceB = responseObject[i][@"optionMap"][@"B"];
      question.choiceC = responseObject[i][@"optionMap"][@"C"];
      question.choiceD = responseObject[i][@"optionMap"][@"D"];
      question.answer = responseObject[i][@"rightkey"];
      [self.questionArr addObject:question];
    }
    [self refreshBtnTitleWithIndex:index];
  }
}

- (void)refreshBtnTitleWithIndex:(NSInteger)index {
  
  self.labelQ.text = [self.questionArr[index] title];
  [self.btnA setTitle:[self.questionArr[index] choiceA] forState:UIControlStateNormal];
  [self.btnB setTitle:[self.questionArr[index] choiceB] forState:UIControlStateNormal];
  [self.btnC setTitle:[self.questionArr[index] choiceC] forState:UIControlStateNormal];
  [self.btnD setTitle:[self.questionArr[index] choiceD] forState:UIControlStateNormal];
}

//从网络请求数据
- (void)requestWithNum:(NSString *) num{
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager GET:[NSString stringWithFormat:@"%@?param=%@", kUrl, self.receive1] parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
    NSLog(@"请求到的数据%@", responseObject);
    NSString *key = [NSString stringWithFormat:@"question%@", self.receive1];
    //将数据存入沙盒
    [[NSUserDefaults standardUserDefaults] setObject:responseObject forKey:key];
    
    [self makeArrWith:responseObject Index:self.questionIndex];
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    
  }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
