//
//  Question.h
//  HelloWorld
//
//  Created by 刘伟 on 16/8/12.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *rightkey;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *choiceA;

@property (nonatomic, copy) NSString *choiceB;

@property (nonatomic, copy) NSString *choiceC;

@property (nonatomic, copy) NSString *choiceD;

@property (nonatomic, copy) NSString *answer;

@end
